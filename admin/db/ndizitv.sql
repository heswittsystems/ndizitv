-- MySQL dump 10.14  Distrib 5.5.56-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: ndizitv
-- ------------------------------------------------------
-- Server version	5.5.56-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mpesa_details`
--

DROP TABLE IF EXISTS `mpesa_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpesa_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `biz_number` int(11) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `mpesa_ref` varchar(50) NOT NULL,
  `kopokopo_ref` varchar(50) NOT NULL,
  `tx_time` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `f_name` varchar(50) NOT NULL,
  `m_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `tx_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpesa_details`
--

LOCK TABLES `mpesa_details` WRITE;
/*!40000 ALTER TABLE `mpesa_details` DISABLE KEYS */;
INSERT INTO `mpesa_details` VALUES (1,1234,'32323','sdfsdf','sdfsdf','34242423','4534534','dfsd','fsdfsd','ffsdf','2018-07-23 10:11:06'),(2,234234,'2342','dgdfg','gdfgdf','fgdfg','34534','dfgdfgd','dfgdfg','dfgdfg','2018-07-23 10:11:27'),(3,0,'','','','','','','','','2018-07-23 16:17:08'),(4,0,'','','','','','','','','2018-07-23 16:21:35'),(5,0,'','','','','','','','','2018-07-23 16:23:59'),(6,0,'','','','','','','','','2018-07-23 16:30:24'),(7,0,'','','','','','','','','2018-07-23 16:30:50'),(8,0,'','','','','','','','','2018-07-23 16:32:51'),(9,0,'','','','','','','','','2018-07-23 16:33:00'),(10,0,'','','','','','','','','2018-07-23 16:39:42'),(11,0,'','','','','','','','','2018-07-23 16:40:16'),(12,0,'','','','','','','','','2018-07-23 16:42:46'),(13,0,'','','','','','','','','2018-07-23 16:44:27'),(14,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','+254715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 16:50:36'),(15,179903,'10.0','MGN5O4BMEZ','37792674','2018-07-23T16:53:52Z','+254715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 16:53:54'),(16,0,'','','','','','','','','2018-07-23 17:17:44'),(17,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','+254715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 17:22:48'),(18,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 17:29:54'),(19,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 17:30:21'),(20,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 17:33:02'),(21,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 17:34:55'),(22,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 18:40:41'),(23,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 18:41:29'),(24,179903,'10.0','MGN3O40ONX','37791301','2018-07-23T16:44:24Z','0715532259','SAMWEL','CHEGE','KAMAU','2018-07-23 18:43:17');
/*!40000 ALTER TABLE `mpesa_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notices`
--

DROP TABLE IF EXISTS `notices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_id` int(11) DEFAULT NULL,
  `notice_data` text,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notices`
--

LOCK TABLES `notices` WRITE;
/*!40000 ALTER TABLE `notices` DISABLE KEYS */;
INSERT INTO `notices` VALUES (1,1,'Thank you for subscribing. Your subscription is valid until August 7, 2018, 6:41 pm','2018-07-23 18:41:29',1),(2,1,'Thank you for subscribing. Your subscription is valid until August 7, 2018, 9:43 pm','2018-07-23 18:43:17',1);
/*!40000 ALTER TABLE `notices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan_details`
--

DROP TABLE IF EXISTS `plan_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_details` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_desc` varchar(200) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `duration_days` int(11) NOT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_details`
--

LOCK TABLES `plan_details` WRITE;
/*!40000 ALTER TABLE `plan_details` DISABLE KEYS */;
INSERT INTO `plan_details` VALUES (1,'test',200,20,'2018-07-23 12:19:34'),(2,'',0,0,'2018-07-23 12:20:29'),(3,'simple',10,15,'2018-07-23 17:22:02');
/*!40000 ALTER TABLE `plan_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL,
  `names` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sub_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
INSERT INTO `subscribers` VALUES (1,'0715532259','Samwel Chege','chegesamwel@gmail.com','81DC9BDB52D04DC20036DBD8313ED055','2018-07-23 17:32:41');
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_id` int(11) NOT NULL,
  `date_start` int(11) DEFAULT NULL,
  `date_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES (1,1,1532367295,1533663295),(2,1,1532371241,1533667241),(3,1,1532371289,1533667289),(4,1,1532371397,1533667397);
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','81dc9bdb52d04dc20036dbd8313ed055','2018-07-23 12:53:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-23 22:38:21
