$(function () {
    $('.js-basic-example').DataTable({
        responsive: true,
	lengthChange: false,
	pageLength: 15,
	order: [[4, 'desc']]
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
