﻿<?php
session_start();
require_once('functions.php');

if($_SESSION['logged'] !== true)
{
	header('Location: login.php');
    exit();
}
date_default_timezone_set('Africa/Nairobi');

extract($_GET);
?>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Ndizi TV payments</title>
    <!-- Favicon-->
    <link rel="icon" href=" favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

	 <!-- Bootstrap Material Datetime Picker Css 
    <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />-->
	<!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
	
    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/theme-green.css" rel="stylesheet" />
</head>

<body class="theme-green">
    <!-- Page Loader 
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>  -->
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
   <!-- <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>-->
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href=" index.html">Ndizi TV Payments</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
              
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['type']; ?></div>
                    <div class="email"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="index.php?id=4"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            
                            <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                   
                    <li>
                        <a href="index.php">
                            <i class="material-icons">home</i>
                            <span>Payments List</span>
                        </a>
                    </li>
					<li>
                        <a href="index.php?id=2">
                            <i class="material-icons">layers</i>
                            <span>Subscribers List</span>
                        </a>
                    </li>
                    <li>
                        <a href="index.php?id=3">
                            <i class="material-icons">text_fields</i>
                            <span>Set Plan Details</span>
                        </a>
                    </li>
                    

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 - 2018 <a href="javascript:void(0);"></a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
       
    </section>
   <?php
  
   if(@!$id)
   {
	   ?>
    <section class="content">
        <div class="container-fluid">
		<!--DateTime Picker 
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Select range to view
                            </h2>
                            
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="datepicker form-control" placeholder="Date from...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="datepicker form-control" placeholder="Date to...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-lineer">
                                          <input type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect" value="View">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!--#END# DateTime Picker -->
          
					 <!-- Striped Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Latest payments                      
                            </h2>                         
                        </div>
                        
						<div class="body table-responsive">
                             <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                           
                                <thead>
                                    <tr>
                                        
                                        <th>MPesa Ref</th>
                                        <th>Phone</th>
                                        <th>Names</th>
                                        <th>Amount</th>
										<th>Date</th>
										
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								$sql = "SELECT * FROM mpesa_details order by id desc";
								$result = mysqli_query($conn, $sql);

								if (mysqli_num_rows($result) > 0) {
									// output data of each row
									while($row = mysqli_fetch_assoc($result)) {
										$names = $row["f_name"]." ". $row["m_name"]." ". $row["l_name"];
										echo '<tr><td>' . $row["mpesa_ref"]. '</td><td>' . $row["phone"]. '</td><td>'. $names. '</td><td>'.$row["amount"].'</td><td>' . $row["tx_date"].'</td></tr>';
									}
								} 
								?>                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
              
			
		</div>	
    </section>
	<?php
   }
   
	
	if(@$id == "2")
	{
		?>
	 <section class="content">
        <div class="container-fluid">
		    					 <!-- Striped Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Subscibers List                              
                            </h2>                         
                        </div>
                        
						<div class="body table-responsive">
                             <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                           
                                <thead>
                                    <tr>
                                        
                                        <th>Names</th>
                                        <th>Phone</th>
                                        <th>Email</th>
										<th>Date Reg</th>
										<th>Sub. Start</th>
										<th>Sub. End</th>
										<th>&nbsp;</th>
										
										
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								$sql = "SELECT * FROM subscribers order by sub_id desc";
								$result = mysqli_query($conn, $sql);
							//	echo mysqli_error($conn);

								if (mysqli_num_rows($result) > 0) {
									// output data of each row 
									while($row = mysqli_fetch_assoc($result)) {
										$det = subs_data($row['sub_id']);
										
										echo '<tr><td>' . $row["names"]. '</td><td>' . $row["phone"]. '</td><td>'. $row["email"]. '</td>
										<td>' . $row["date_reg"].'</td><td>'.$det[0].'</td><td>'.$det[1].'</td><td><a href=index.php?id=5&do=edit&sub_id='.$row['sub_id'].'&name='.urlencode($row["names"]).'>edit</a></td></tr>';
									}
								} 
								?>                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
              
			
		</div>	
    </section>
    <?php
	}
	if(@$id == "5")
	{
		
		?>
		
		<section class="content">
        <div class="container-fluid">
          
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                     
                        <div class="body">
                          <form action="" method="post"> 
                            <h2 class="card-inside-title">Update Subscription details</h2>
                            <div class="row clearfix">
                               
								
								<div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="r_date" class="datepicker form-control" >
											 <label class="form-label">Subscription date start</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="exp_date" class="datepicker form-control" >
											 <label class="form-label">Subscription date end</label>
                                        </div>
                                    </div>
                                </div>
								
								<input type=hidden name="sub_id" value=<?php echo $_GET['sub_id'];?>>
								 <input type="submit" class="btn btn-primary m-t-15 waves-effect" value="Add">
                                
                            </div>
						 </form>	
						 <?php
						 if($_POST)
						 {
							 extract($_POST);
							 $start = strtotime($r_date);
							 $end = strtotime($exp_date);
							 $sql = "delete from subscriptions where sub_id='$sub_id'";
							 mysqli_query($conn,$sql);
							 
							 $sql = "insert into subscriptions set date_start='$start' , date_end='$end',sub_id='$sub_id'";
							 if(mysqli_query($conn,$sql))
							 {
								?><div class="alert alert-success">
                                <strong>Success.</strong> Data successfully updated.
                                </div><?php
                            
							}
							else
							{
								?><div class="alert alert-danger">
                                <strong>Error. </strong> Data not posted.
                                </div>
							<?php
							}
							 
						 }	 
                         ?>   
                        </div>
                    </div>
		
                </div>
            </div>
			
		</div>	
    </section>
		
		<?php
		
	}
	if(@$id == "3")
	{
		?>
	  <section class="content">
        <div class="container-fluid">
          
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                     
                        <div class="body">
                          <form action="" method="post"> 
                            <h2 class="card-inside-title">Add Subscription details</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" required name="desc" class="form-control">
                                            <label class="form-label">Plan Name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" required name="amount" class="form-control">
                                            <label class="form-label">Amount</label>
                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" required name="days" class="form-control">
                                            <label class="form-label">Duration in days</label>
                                        </div>
                                    </div>
                                </div>
								 <input type="submit" class="btn btn-primary m-t-15 waves-effect" value="Add">
                                
                            </div>
						 </form>	
						<?php
						    

						if($_POST)
						{
							extract($_POST);
							if(set_sub_duration($desc,$amount,$days))
							{
								?><div class="alert alert-success">
                                <strong>Success.</strong> Data successfully added.
                                </div><?php
                            
							}
							else
							{
								?><div class="alert alert-danger">
                                <strong>Error. </strong> Data not posted.
                                </div>
							<?php
							}
						
							
						}

						?>
                            
                        </div>
                    </div>
					 <!-- Striped Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Current Plans                              
                            </h2>                         
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Plan Name</th>
                                        <th>Amount </th>
										<th>Duration Days </th>
                                        <th>Date Added</th>
										 <th>&nbsp;</th>
										
										 
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								if($do == "del")
								{
									$sql = "delete FROM plan_details where plan_id = '$pid'";
								    $result = mysqli_query($conn, $sql);
								}
								$sql = "SELECT * FROM plan_details order by plan_id";
								$result = mysqli_query($conn, $sql);
							    $j = 1;
								if (mysqli_num_rows($result) > 0) {
									// output data of each row
									while($row = mysqli_fetch_assoc($result)) {
										echo '<th scope="row"> '. $j.'</th><td>'. $row["plan_desc"]. '</td><td>' . $row["amount"]. '</td><td>' . $row["duration_days"].'</td><td>'. $row["date_reg"]. '</td><td><a href=index.php?id=3&do=del&pid='.$row["plan_id"].'>delete</td></tr>';
										$j++;
									}
								} 
								?>                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
			
		</div>	
    </section>
    <?php
	}
	if(@$id == "4")
	{
		?>
	  <section class="content">
        <div class="container-fluid">
          
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                     
                        <div class="body">
                          <form action="" method="post"> 
                            <h2 class="card-inside-title">Change Password</h2>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" name="passwd" class="form-control">
                                            <label class="form-label">Current Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" required name="passwd1" class="form-control">
                                            <label class="form-label">New Password</label>
                                        </div>
                                    </div>
                                </div>
								<div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" required name="passwd2" class="form-control">
                                            <label class="form-label">Confirm Password</label>
                                        </div>
                                    </div>
                                </div>
								 <input type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect" value="Update">
                                
                            </div>
						 </form>	
						<?php
						    

						if($_POST)
						{
							
							extract($_POST);
							if(strtoupper($passwd1) != strtoupper($passwd2))
							{
								?><div class="alert alert-danger">
                                <strong>Error. </strong> Passwords do not match.
                                </div>
							<?php
							   	
							}
							else
							{								
								$password = md5($passwd);
								$sql = "SELECT * from users where username='$username' and user_id='".$_SESSION['user_id']."' ";
								$result = mysqli_query($conn, $sql);
								if (mysqli_num_rows($result) > 0) {
								// output data of each row
								   $pw = md5($passwd1);
								   $sql = "update users set password='$pw' where user_id='".$_SESSION['user_id']."'";
								   if(mysqli_query($conn, $sql))
								   {
									   ?><div class="alert alert-success">
										<strong>Success.</strong> Data successfully added.
										</div>
										<?php
								    }
								    else
									{
										?><div class="alert alert-danger">
										<strong>Error. </strong> data not saved
										</div>
										<?php
									}
                                  									
									   								   
								}
								else
								{
									?><div class="alert alert-danger">
                                    <strong>Error. </strong> Wrong Username or Password
                                    </div>
							        <?php
								}
							}
						}

						?>
                            
                        </div>
                    </div>
					 <!-- Striped Rows -->
            
                </div>
            </div>
			
		</div>	
    </section>
    <?php
	}
	?>
    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- Autosize Plugin Js -->
    <script src="plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="plugins/momentjs/moment.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

	 <script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	 
	<script src=" plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src=" plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
   


    <!-- Custom Js -->
     <script src="js/pages/tables/jquery-datatable.js"></script>
	
    <script src="js/pages/forms/basic-form-elements.js"></script>
	
    <!-- Custom Js -->
    <script src="js/admin.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>

</html>
