<?php
session_start();

if($_POST)
{
	require_once('admin/functions.php');
	require_once('admin/functions.php');
	extract($_POST);
	if(login_sub($username,$password))
	{
		if(!check_active_sub($_SESSION['sub_id']))//no active subscription
		{		
			header('Location: index.php?ttr=2');			
			exit();
		}
		else
		{
			header('Location: dashboard-home.php');			
			exit();
			
		}
		
	}
	else
	{
		?>
			<div class="alert alert-danger">
			<strong>Error. </strong> Wrong username or password
			</div>
			<?php
		
	}
	
}
if($_GET['ttr'] == "2")
{
	?>
			<div class="alert alert-danger">
			<strong>Error. </strong> Please subscribe to enjoy our content
			</div>
			<?php
}

?>
<html lang="en">
	

<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">
		
		<link rel="stylesheet" href="icons/fontawesome/css/fontawesome-all.min.css"><!-- FontAwesome Icons -->
		<link rel="stylesheet" href="icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css"><!-- iconsmind.com Icons -->
		
		<title>Ndizi Tv </title>
	</head>
	<body>
		<header id="masthead-pro">
			<div class="container">
				
				<h1><a href="#"><img src="images/logo.png" alt="Logo"></a></h1>
				
				<nav id="site-navigation-pro">
					<ul class="sf-menu">
						<li class="normal-item-pro current-menu-item">
							<a href="videos/index.mp4">Home</a>
						</li>
						<li class="normal-item-pro">
							<a href="dashboard-home.html">New Releases</a>
							<!-- Sub-Menu Example >
							<ul class="sub-menu">
								<li class="normal-item-pro">
									<a href="#!">Sub-menu item 1</a>
								</li>
								<li class="normal-item-pro">
									<a href="#!">Sub-menu item 2</a>
								</li>
								<li class="normal-item-pro">
									<a href="#!">Sub-menu item 3</a>
								</li>
							</ul> 
							< End Sub-Menu example -->
						</li>
						<li class="normal-item-pro">
							<a href="signup-step1.php">Pricing Plans</a>
						</li>
						<li class="normal-item-pro">
							<a href="faqs.html">FAQs</a>
						</li>
					</ul>
				</nav>
				
				<button class="btn btn-header-pro noselect" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</button>
				
				<div id="mobile-bars-icon-pro" class="noselect"><i class="fas fa-bars"></i></div>
				
				<div class="clearfix"></div>
			</div><!-- close .container -->
			
			<nav id="mobile-navigation-pro">
			
				<ul id="mobile-menu-pro">
					<li>
						<a href="#">Home</a>
					</li>
					<li>
						<a href="dashboard-home.html">New Releases</a>
						<!-- Mobile Sub-Menu Example >
						<ul>
							<li class="normal-item-pro">
								<a href="#!">Sub-menu item 1</a>
							</li>
							<li class="normal-item-pro">
								<a href="#!">Sub-menu item 2</a>
							</li>
							<li class="normal-item-pro">
								<a href="#!">Sub-menu item 3</a>
							</li>
						</ul>
						< End Mobile Sub-Menu Example -->
					</li>
					<li>
						<a href="signup-step1.html">Pricing Plans</a>
					</li>
					<li>
						<a href="faqs.html">FAQs</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			
				<button class="btn btn-mobile-pro btn-green-pro noselect" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</button>
			
			</nav>
		</header>
		
		
		
		
		<div class="flexslider progression-studios-slider">
	      <ul class="slides">
				<li class="progression_studios_animate_in">
					<div class="progression-studios-slider-image-background" style="background-image:url(images/demo/slide-1.jpg);">
						<div class="progression-studios-slider-display-table">
							<div class="progression-studios-slider-vertical-align">
								
								<div class="container">
									
									<div class="progression-studios-slider-caption-width">
										<div class="progression-studios-slider-caption-align">
											<h2>Welcome to Ndizi<span style="color:#3db13d;">TV</span></h2>
											<h6>Ndizi TV brings engaging stories to movie lovers around the world. We have high quality Short films, feature films, documentaries, web series, comedy clips and classic TV, all made my Kenyan producers.</h6>
											<a class="btn btn-green-pro btn-slider-pro btn-shadow-pro" href="signup-step1.html" role="button">Start Your Free Trial</a>
										</div><!-- close .progression-studios-slider-caption-align -->
									</div><!-- close .progression-studios-slider-caption-width -->
									
								</div><!-- close .container -->
								
							</div><!-- close .progression-studios-slider-vertical-align -->
						</div><!-- close .progression-studios-slider-display-table -->
						
						<div class="progression-studios-slider-mobile-background-cover"></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
				<li class="progression_studios_animate_right">
					<div class="progression-studios-slider-image-background" style="background-image:url(images/demo/slide-2.jpg); background-color:#111015;">
						<div class="progression-studios-slider-display-table">
							<div class="progression-studios-slider-vertical-align">
								
								<div class="container">
									
									<div class="progression-studios-slider-caption-width">
										<div class="progression-studios-slider-caption-align">
											<h2 class="light-fonts-pro">Foundation </h2>
											<h6 class="light-fonts-pro">Founded by film makers, Ndizi TV is an almost free streaming service that's keen on making good quality content available to an eager audience at a small fee. </h6>
											<a class="btn btn-green-pro btn-slider-pro" href="dashboard-home.html" role="button">View The Video Library</a>
										</div><!-- close .progression-studios-slider-caption-align -->
									</div><!-- close .progression-studios-slider-caption-width -->
									
								</div><!-- close .container -->
								
							</div><!-- close .progression-studios-slider-vertical-align -->
						</div><!-- close .progression-studios-slider-display-table -->
						
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
				<li class="progression_studios_animate_left">
					<div class="progression-studios-slider-image-background" style="background-image:url(images/demo/slide-3.jpg);">
						<div class="progression-studios-slider-display-table">
							<div class="progression-studios-slider-vertical-align">
								
								<div class="container">
									
									<div class="progression-studios-slider-caption-width">
										<div class="progression-studios-slider-caption-align">
											<h2>Who we are </h2>
											<h6>Ndizi TV is a pioneer in the industry, paying film makers for every minute their content is watched in a win-win model that increases film makers' payout as the company grows. Our major revenue stream is from advertisements, which are keenly selected by our curators so as to match the audience profiling.</h6>
											<a class="btn btn-green-pro btn-slider-pro btn-shadow-pro" href="signup-step1.html" role="button">Start Your Free Trial</a>
										</div><!-- close .progression-studios-slider-caption-align -->
									</div><!-- close .progression-studios-slider-caption-width -->
									
								</div><!-- close .container -->
								
							</div><!-- close .progression-studios-slider-vertical-align -->
						</div><!-- close .progression-studios-slider-display-table -->
						
						<div class="progression-studios-slider-mobile-background-cover"></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
			</ul>
		</div><!-- close .progression-studios-slider - See /js/script.js file for options -->
		
		<div id="content-pro">
			
  	 		<div class="container">
				

				<div class="row">
					<div class="col-md my-auto"><!-- .my-auto vertically centers contents -->
						<img src="images/demo/home-1.jpg" class="img-fluid" alt="Watch in Any Devices">
					</div>
					<div class="col-md my-auto"><!-- .my-auto vertically centers contents -->
	  					<h2 class="short-border-bottom">Watch On Any Device</h2>
						<p>We are in  the process of creating a mobile powered application that will help you access mobile content to go, we have a wide range of ideas and stories we would like to bring to you. </p>
						<div style="height:15px;"></div>
						<p><a class="btn btn-green-pro" href="signup-step1.php" role="button">Learn More</a></p>
					</div>
				</div><!-- close .row -->
				
				
				<div class="row">
					<div class="col-md my-auto"><!-- .my-auto vertically centers contents -->
	  					<h2 class="short-border-bottom">Binge Watch </h2>
						<p>We are giving you a platform where you can watch all of our webisodes from the comfort of your smart device  </p>
						<div style="height:15px;"></div>
						<p><a class="btn btn-green-pro" href="signup-step1.php" role="button">Start Watching</a></p>
					</div>
					<div class="col-md my-auto"><!-- .my-auto vertically centers contents -->
						<img src="images/demo/home-2.jpg" class="img-fluid" alt="Binge Watch">
					</div>
				</div><!-- close .row -->
				
				
				<div class="row">
					<div class="col-md my-auto"><!-- .my-auto vertically centers contents -->
						<img src="images/demo/home-3.jpg" class="img-fluid" alt="Watch in Ultra HD">
					</div>
					<div class="col-md my-auto"><!-- .my-auto vertically centers contents -->
	  					<h2 class="short-border-bottom">Watch in Ultra HD</h2>
						<p>We are also incorporating high end 4k for high end mobile and smart devices </p>
						<div style="height:15px;"></div>
						<p><a class="btn btn-green-pro" href="signup-step1.php" role="button">Start Your Free Trial</a></p>
					</div>
				</div><!-- close .row -->
				
				<div style="height:35px;"></div>
				
				<div class="clearfix"></div>
			</div><!-- close .container -->
			
			
			<hr>
			
			<div class="progression-pricing-section-background">
			<div class="container">
				
				<div class="row">
					<div class="mx-auto">
						<div style="height:70px;"></div>
						<h2 class="short-border-bottom">Our Plans &amp; Pricing</h2>
					</div>
				</div><!-- close .row -->
				
				<div style="height:25px;"></div>
				
				<div class="row">
					<div class="col-md">
						<ul class="checkmark-list-pro">
							<li>1 month unlimited access!</li>
							<li>Thousands of TV shows, movies &amp; more.</li>
						</ul>
					</div>
					<div class="col-md">
						<ul class="checkmark-list-pro">
							<li>Stream on your phone, laptop, tablet or TV.</li>
							<li>You can even Download & watch offline.</li>
						</ul>
					</div>
					<div class="col-md">
						<ul class="checkmark-list-pro">
							<li>1 month unlimited access!</li>
							<li>Thousands of TV shows, movies &amp; more.</li>
						</ul>
					</div>
				</div><!-- close .row -->
				
				
				<div class="pricing-table-pro">
					<div class="row">
						<div class="col-md">
							<div class="pricing-table-col">
								<h6>FREE TRIAL</h6>
								<h2>Free</h2>
								<ul>
									<li>720p Available</li>
									<li>Watch on any Device</li>
									<li>20 Movies and Shows</li>
									<li>Download Available</li>
								</ul>
								<p><a class="btn" href="signup-step2.php" role="button">Choose Plan</a></p>
							</div><!-- close .pricing-table-col -->
						</div><!-- close .col-md-12 -->
						<div class="col-md">
							<div class="pricing-table-col pricing-table-col-shadow-pro">
								<h6>STARTER</h6>
								<h2><sup> Kes </sup>50<span> / month</span></h2>
								<ul>
									<li>HD Available</li>
									<li>Watch on laptop </li>
									<li>10 Movies and Shows</li>
									<li>Download Available</li>
								</ul>
								<p><a class="btn btn-green-pro" href="signup-step2.php" role="button">Choose Plan</a></p>
							</div><!-- close .pricing-table-col -->
						</div><!-- close .col-md-12 -->
						<div class="col-md">
							<div class="pricing-table-col">
								<h6>PREMIUM</h6>
								<h2><sup>kes</sup>200<span> / month</span></h2>
								<ul>
									<li>Ultra HD Available</li>
									<li>Watch on any Device</li>
									<li>Unlimited Movies and Shows</li>
									<li>Download Available</li>
								</ul>
								<p><a class="btn" href="signup-step2.php" role="button">Choose Plan</a></p>
							</div><!-- close .pricing-table-col -->
						</div><!-- close .col-md-12 -->
					</div><!-- close .row -->
				</div><!-- close .pricing-table-pro -->
					
				
				<div class="clearfix"></div>
			</div><!-- close .container -->
			
			</div><!-- close .progression-pricing-section-background -->
			
		</div><!-- close #content-pro -->
		
		<footer id="footer-pro">
			<div class="container">
				<div class="row">
					<div class="col-md">
						<div class="copyright-text-pro">&copy; Copyright 2018 Ndizi Tv   All Rights Reserved
						<span> Design by <a href="http://ignatius.me.ke/"> Ignatius Designs </a>  </span> </div>
					</div><!-- close .col -->
					<div class="col-md">
						<ul class="social-icons-pro">
							<li class="facebook-color"><a href="https://www.facebook.com/pg/ndizitv" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li class="twitter-color"><a href="https://twitter.com/NdiziTv" target="_blank"><i class="fab fa-twitter"></i></a></li>
							<li class="youtube-color"><a href="https://www.youtube.com/channel/UCBKm4VBZRvHrR-Qj5nWToJw" target="_blank"><i class="fab fa-youtube"></i></a></li> 
							<li class="vimeo-color"><a href="http://vimeo.com/" target="_blank"><i class="fab fa-vimeo-v"></i></a></li>
						</ul>
					</div><!-- close .col -->
				</div><!-- close .row -->
			</div><!-- close .container -->
		</footer>
		
		<a href="#0" id="pro-scroll-top"><i class="fas fa-chevron-up"></i></a>
		
		
		<!-- Modal -->
		<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
			 <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
			           <span aria-hidden="true">&times;</span>
			</button>
		  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
		    <div class="modal-content">
				 <div class="modal-header-pro">
					 <h2>Welcome Back</h2>
					 <h6>Sign in to your account to continue using Ndizi TV </h6>
				 </div>
				 <div class="modal-body-pro social-login-modal-body-pro">
			 
					 <div class="registration-social-login-container">
						 <form action="index.php" method="post">
							 <div class="form-group">
								 <input type="email" required name="username" class="form-control" id="username" placeholder="Email">
							 </div>
							 <div class="form-group">
								 <input type="password" required name="password" class="form-control" id="password" placeholder="Password">
							 </div>
							 <div class="form-group">
								 <input type="submit" class="btn btn-green-pro btn-display-block" name="login" value="Sign In">
							 </div>
							 <div class="container-fluid">
								 <div class="row no-gutters">
							 		<div class="col checkbox-remember-pro"><input type="checkbox" id="checkbox-remember"><label for="checkbox-remember" class="col-form-label">Remember me</label></div>
									<div class="col forgot-your-password"><a href="#!">Forgot your password?</a></div>
								</div>
							</div><!-- close .container-fluid -->
				
						 </form>
			 <!---
						 <div class="registration-social-login-or">or</div>
				 -->
					 </div>
<!-- close .registration-social-login-container -->
			 
					 <!-- <div class="registration-social-login-options"> 
					 	<!-- <h6>Sign in with your social account</h6> 
						<!-- <div class="social-icon-login facebook-color"><i class="fab fa-facebook-f"></i> Facebook</div>
						<!-- <div class="social-icon-login twitter-color"><i class="fab fa-twitter"></i> Twitter</div> -->
						<!-- <div class="social-icon-login google-color"><i class="fab fa-google-plus-g"></i> Google</div> -->
					 <!-- </div><!-- close .registration-social-login-options --> 
			 
					 <div class="clearfix"></div>
			 

 		      </div><!-- close .modal-body -->
		
			 <a class="not-a-member-pro" href="signup-step2.php">Not a member? <span>Join Today!</span></a>
		    </div><!-- close .modal-content -->
		  </div><!-- close .modal-dialog -->
		</div><!-- close .modal -->
		

		<!-- Required Framework JavaScript -->
		<script src="js/libs/jquery-3.3.1.min.js"></script><!-- jQuery -->
		<script src="js/libs/popper.min.js" defer></script><!-- Bootstrap Popper/Extras JS -->
		<script src="js/libs/bootstrap.min.js" defer></script><!-- Bootstrap Main JS -->
		<!-- All JavaScript in Footer -->
		
		<!-- Additional Plugins and JavaScript -->
		<script src="js/navigation.js" defer></script><!-- Header Navigation JS Plugin -->
		<script src="js/jquery.flexslider-min.js" defer></script><!-- FlexSlider JS Plugin -->		
		<script src="js/script.js" defer></script><!-- Custom Document Ready JS -->
		
	</body>


</html>